fun main(args: Array<String>) {
    var i: Int = 1
    while (i != 2) {
        ++i
    }
    do {
        --i
    } while (i > 0)
    println(i)
}